# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2011 OpenERP S.A (<http://www.openerp.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import  osv,fields

class account_tax_i(osv.Model):
	"""
		Add parameterization to manipulate tax invoice.
	"""
	_name='account.tax'
	_inherit = 'account.tax'

	_columns = {
		'evaluate_invoince' : fields.boolean('Evaluete Invoice', help=""),
		'affect_balance' : fields.boolean('Not Affect Balance',help=""),
		'show_invoice' : fields.boolean('Not Show in Invoice',help=""),
		'account_tax_id' : fields.many2one('account.account', 'Tax Account', help=""),
		'account_tax_repayment_id' : fields.many2one('account.account', 'Account Tax repayment', help=""),		
	}

	"""
	_defaults={

	}
	"""


account_tax_i()
